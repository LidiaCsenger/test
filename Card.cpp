#include "Card.h"

void Card::PrintCard()
{
	switch (m_number)
	{
	case one:
		std::cout << "One ";
		break;
	case two:
		std::cout << "Two ";
		break;
	case three:
		std::cout << "Three ";
		break;
	}
	switch (m_symbol)
	{
	case diamond:
		std::cout << "Diamond ";
		break;
	case squiggle:
		std::cout << "Squiggle ";
		break;
	case oval:
		std::cout << "Oval ";
		break;
	}
	switch (m_shading)
	{
	case solid:
		std::cout << "Solid ";
		break;
	case striped:
		std::cout << "Striped ";
		break;
	case open:
		std::cout << "Open ";
		break;
	}
	switch (m_color)
	{
	case red:
		std::cout << "Red "<<std::endl;
		break;
	case green:
		std::cout << "Green " << std::endl;
		break;
	case blue:
		std::cout << "Blue " << std::endl;
		break;
	}
}

