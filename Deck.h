#pragma once
#include "Card.h"
#include <array>
class Deck
{
public:
	Deck(char* n);
	void PrintDeck();
	void ShufleDeck();
private:
	int m_index;
	char* m_nume;
	std::array<Card, 81> m_deck;
};