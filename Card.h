#pragma once
#include <iostream>
class Card
{
public:
	enum Number
	{
		one,
		two,
		three
	};
	enum Symbol
	{
		diamond,
		squiggle,
		oval
	};
	enum Shading
	{
		solid,
		striped,
		open
	};
	enum Color
	{
		red,
		green,
		blue
	};

	Card(){}
	Card(Number n,Symbol s,Shading sh,Color c):m_number(n),m_symbol(s),m_shading(sh),m_color(c){}
	void PrintCard();
private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;
};