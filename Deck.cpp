#include "Deck.h"
#include <random>
std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<>dis(0, 80);
Deck::Deck(char* n):m_nume(n)
{
	m_index = 0;
	int index = 0;
	for (int i = Card::Number::one; i < 3; i++)
		for (int j = Card::Symbol::diamond; j < 3; j++)
			for (int k = Card::Shading::solid; k < 3; k++)
				for (int l = Card::Color::red; l < 3; l++)
			      m_deck[index++] = Card(static_cast<Card::Number>(i), static_cast<Card::Symbol>(j),
					  static_cast<Card::Shading>(k), static_cast<Card::Color>(l));
}
void Deck::PrintDeck()
{
		for each (Card c in m_deck)
		{
			c.PrintCard();
		}
}
void Deck::ShufleDeck()
{
	m_index = 0;
	int i = 0;
	Card aux;
	for each (Card c in m_deck)
	{
		int swapIndex = dis(gen);
		aux = m_deck[i];
		m_deck[i] = m_deck[swapIndex];
		m_deck[swapIndex] = aux;
		i++;
	}
}